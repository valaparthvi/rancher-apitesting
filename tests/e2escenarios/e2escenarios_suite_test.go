package e2escenarios_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestE2escenarios(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "E2escenarios Suite")
}
