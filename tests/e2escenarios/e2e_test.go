package e2escenarios_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/tidwall/gjson"

	"github.com/valaparthvi/rancher-apitesting/tests/helper"
)

var _ = Describe("E2e", func() {
	var loginHandler helper.LoginHandler
	BeforeEach(func() {
		loginHandler = helper.CommonSetupBeforeEach()
	})
	Context("Login to the cluster with username and password", func() {
		It("should successfully connect the API via username and password", func() {
			body := helper.GetHTTPResponse(loginHandler.URL, loginHandler.Username, loginHandler.Password)
			Expect(body).ToNot(BeEmpty())
			Expect(gjson.Get(body, "serverAddressByClientCIDRs.#")).ToNot(BeZero())
		})
	})
})
