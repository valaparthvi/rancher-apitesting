package helper

import (
	"fmt"
	"os"

	. "github.com/onsi/gomega"
)

type LoginHandler struct {
	URL, Username, Password string
}

// CommonSetupBeforeEach runs the common setup that should be run before each test and returns the LoginHandler
func CommonSetupBeforeEach() LoginHandler {
	url := os.Getenv("TEST_URL")
	Expect(url).ToNot(BeEmpty(), "URL is not set. Export the URL as an environment variable via TEST_URL")

	username := os.Getenv("TEST_USERNAME")
	Expect(username).ToNot(BeEmpty(), "Username is not set. Export the username as an environment variable via TEST_USERNAME")

	password := os.Getenv("TEST_PASSWORD")
	Expect(password).ToNot(BeEmpty(), "Password is not set. Export the password as an environment variable via TEST_PASSWORD")

	return LoginHandler{
		URL:      fmt.Sprintf("%s/api", url),
		Username: username,
		Password: password,
	}
}
