package helper

import (
	"crypto/tls"
	"io"
	"net/http"

	. "github.com/onsi/gomega"
)

// GETHTTPResponse takes URL, username, and password and returns the response body in string format
func GetHTTPResponse(url, username, password string) string {
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	request, err := http.NewRequest("GET", url, nil)
	Expect(err).ToNot(HaveOccurred())
	request.SetBasicAuth(username, password)

	response, err := client.Do(request)
	Expect(err).ToNot(HaveOccurred())
	defer response.Body.Close()

	Expect(response.StatusCode).To(BeEquivalentTo(http.StatusOK))

	body, err := io.ReadAll(response.Body)
	Expect(err).ToNot(HaveOccurred())
	return string(body)
}
