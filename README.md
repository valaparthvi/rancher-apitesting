## API E2E Testing for Rancher
Implement e2e API test for single node install of Rancher UI
Test to cover.
- Login into Rancher

Install Rancher UI:
```shell
docker run --privileged -d --restart=unless-stopped -p 8080:80 -p 8443:443 rancher/rancher
```

1. Access the URL on https://localhost:8443.
2. Follow the instruction to login.
3. Follow [these instructions](https://ranchermanager.docs.rancher.com/pages-for-subheaders/about-the-api#how-to-use-the-api) to generate API Keys.
4. Export `TEST_URL=https://localhost:8443`; `TEST_USERNAME=<api-access-key>`; `TEST_PASSWORD=<api-secret-key>`.
5. `cd tests/e2escenarios`
6. `ginkgo`