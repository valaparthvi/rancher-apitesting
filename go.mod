module github.com/valaparthvi/rancher-apitesting

go 1.16

require (
	github.com/onsi/ginkgo/v2 v2.9.4 // indirect
	github.com/onsi/gomega v1.27.6 // indirect
	github.com/tidwall/gjson v1.14.4 // indirect
)
